/*
 * gpc_handlers.h
 *
 * host and sw_kernel library
 *
 * Macro instantiation for handlers
 * !!! This file should be identical for sw_kernel and host libraries !!!
 *
 * Created on: April 23, 2021
 * Author: A.Popov
 */
#ifndef DEF_HANDLERS_H_
#define DEF_HANDLERS_H_
#define DECLARE_EVENT_HANDLER(handler) \
            const unsigned int event_ ## handler =__LINE__; \
            void handler ();
#define __event__(handler) event_ ## handler
//  Event handlers declarations by declaration line number!!! 
DECLARE_EVENT_HANDLER(test0);
DECLARE_EVENT_HANDLER(test1);
DECLARE_EVENT_HANDLER(test2);
DECLARE_EVENT_HANDLER(test3);
DECLARE_EVENT_HANDLER(test4);
DECLARE_EVENT_HANDLER(test5);
DECLARE_EVENT_HANDLER(test6);
DECLARE_EVENT_HANDLER(test7);
DECLARE_EVENT_HANDLER(test8);
DECLARE_EVENT_HANDLER(test9);
DECLARE_EVENT_HANDLER(test10);
DECLARE_EVENT_HANDLER(test11);
DECLARE_EVENT_HANDLER(test12);
DECLARE_EVENT_HANDLER(test13);
DECLARE_EVENT_HANDLER(test14);
DECLARE_EVENT_HANDLER(test15);
DECLARE_EVENT_HANDLER(exp1);
DECLARE_EVENT_HANDLER(exp2);
DECLARE_EVENT_HANDLER(exp3);
DECLARE_EVENT_HANDLER(exp4);
DECLARE_EVENT_HANDLER(exp5);
DECLARE_EVENT_HANDLER(exp6);
DECLARE_EVENT_HANDLER(exp7);
DECLARE_EVENT_HANDLER(exp8);
DECLARE_EVENT_HANDLER(exp9);
DECLARE_EVENT_HANDLER(exp10);
DECLARE_EVENT_HANDLER(frequency_measurement);
DECLARE_EVENT_HANDLER(get_lnh_status_low);
DECLARE_EVENT_HANDLER(get_lnh_status_high);
DECLARE_EVENT_HANDLER(get_version);
DECLARE_EVENT_HANDLER(mq_rate_test);
DECLARE_EVENT_HANDLER(buf_rate_test);
DECLARE_EVENT_HANDLER(external_memory_test);


#endif
